<?php

Route::get('reset-password', 'App@view')->name('password.reset');

Route::get('{all}', 'App@view')->where('all', '.*');