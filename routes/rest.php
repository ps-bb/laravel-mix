<?php

Route::get(         'user',                 'UserController@get' );

Route::middleware('guest')->group(function() {
	Route::post(    'user',                 'UserController@signIn' );
	Route::post(    'user/sign-up',         'UserController@signUp' );
	Route::post(    'user/forgot',          'UserController@forgot' );
	Route::post(    'user/reset',           'UserController@reset' );
});

Route::middleware('auth')->group(function() {
	Route::delete(  'user',                 'UserController@logout' );
	Route::put(     'user/password',        'UserController@password' );
	Route::post(    'user/logo',            'UserController@logo' );
});

Route::middleware('can:admin')->group(function() {

});

Route::any('{all}', 'App@notFound')->where([ 'all' => '.*' ]);
