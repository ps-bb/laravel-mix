<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class UserTest extends TestCase
{
	private $user = [
		'email' => 'UserTest@test.test',
		'logo' => null,
		'logoUrl' => null,
		'role' => 'Member'
	];

	function testAuth()
	{
		$this->get('/rest/user')
		     ->assertSee('[]');

		$this->post('/rest/user/sign-up', [
			'email' => $this->user['email'],
			'password' => 11111111,
			'password_confirmation' => 11111111
		])->assertJsonFragment($this->user);

		$this->get('/rest/user')
			 ->assertJsonFragment($this->user);

		$this->delete('/rest/user')
		     ->assertOk();

		$this->get('/rest/user')
		     ->assertSee('[]');
	}

	function testForgot()
	{
		$this->post('/rest/user/forgot', [ 'email' => $this->user['email'] ])
		     ->assertJsonFragment([ 'message' => "We have e-mailed your password reset link!" ]);

		$this->assertDatabaseHas('password_resets', [ 'email' => $this->user['email'] ]);
	}

	function testPassword()
	{
		auth()->onceUsingId(4);

		$this->put('/rest/user/password', [
			'password' => 22222222,
			'password_confirmation' => 22222222
		])->assertJsonFragment(array_merge($this->user, [
			'message' => 'Password successfully changed',
		]));
	}

	function testLogo()
	{
		auth()->onceUsingId(4);

		$this->post('/rest/user/logo', [ 'logo' => UploadedFile::fake()->image('avatar.png') ])
		     ->assertJsonFragment([ 'message' => 'Logo successfully changed' ]);
	}

	function testLogoRemove()
	{
		auth()->onceUsingId(4);

		$this->post('/rest/user/logo')
		     ->assertJsonFragment([ 'message' => 'Logo successfully deleted' ]);
	}

}
