<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Contracts\Console\Kernel;

abstract class TestCase extends BaseTestCase
{
	static $migrate = true;

	/**
	 * Setup the test environment.
	 *
	 * @return void
	 */
	function setUpTraits()
	{
		parent::setUpTraits();
		if ( self::$migrate ) {
			$this->artisan('migrate:fresh');
			$this->seed();
			self::$migrate = false;
		}
	}

	/**
	 * Creates the application.
	 *
	 * @return \Illuminate\Foundation\Application
	 */
	public function createApplication()
	{
		$app = require __DIR__.'/../bootstrap/app.php';

		$app->make(Kernel::class)->bootstrap();

		return $app;
	}

}
