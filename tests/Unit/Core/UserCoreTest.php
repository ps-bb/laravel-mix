<?php

namespace Tests\Feature\Controllers;

use App\Model\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Core\UserCore;

class UserCoreTest extends TestCase
{

	private $model = null;
	private $user = [
		'email' => 'UserCoreTest@test.test',
		'logo' => null,
		'logoUrl' => null,
		'role' => 'Member'
	];

	public function __construct( $name = null, array $data = [], $dataName = '' ) {
		parent::__construct( $name, $data, $dataName );
		$this->model = new UserCore;
	}

	function testGet()
	{
		$this->assertNull(
			$this->model->get()
		);

		$this->assertNull(
			$this->model->get('random', 1)
		);

		$this->assertEquals(
			'Admin',
			$this->model->get('role', 1)
		);
	}

	function testAttempt()
	{
		$this->assertFalse(
			$this->model->attempt('random', 'random')
		);

		$this->assertTrue(
			$this->model->attempt('ps.dev.smtp@gmail.com', 11111111)
		);
	}

	function testSignUp()
	{
		$this->assertTrue(
			isset($this->model->signUp('UserCoreTest@gmail.com', 11111111)->id)
		);

		try {
			$this->model->signUp('UserCoreTest@gmail.com', 11111111);
			$this->assertTrue(false);
		} catch (\Illuminate\Database\QueryException $e) {
			$this->assertTrue(true);
		}

	}

	function testResetPassword()
	{
		$this->assertFalse(
			$this->model->sendResetPassword('random')
		);

		$this->assertTrue(
			$this->model->sendResetPassword('ps.dev.smtp@gmail.com')
		);

		$this->assertDatabaseHas('password_resets', [ 'email' => 'ps.dev.smtp@gmail.com' ]);
	}

	function testSetPassword()
	{
		$this->assertFalse(
			$this->model->setPassword(11111111)
		);

		$this->assertFalse(
			$this->model->setPassword(1111, 4)
		);

		$this->assertTrue(
			$this->model->setPassword(11111111, 4)
		);
	}

	function testUploadLogo()
	{
		$this->assertTrue(
			$this->model->uploadLogo(
				UploadedFile::fake()->image('avatar.png'),
				4
			)
		);
	}

	function testDeleteLogo()
	{
		$this->assertTrue(
			$this->model->deleteLogo(4)
		);
	}
}
