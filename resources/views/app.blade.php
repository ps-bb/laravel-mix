<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laravel Vue</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes">
    <meta name="designer" content="Designed with Soul and Care by Web Design Sun® | https://www.webdesignsun.com">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('assets/app.css') }}">
    <script nomodule src="/assets/polyfill.js"></script>
    <script defer src="{{ mix('assets/app.js') }}"></script>
</head>
<body></body>
</html>