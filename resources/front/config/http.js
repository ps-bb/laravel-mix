import Vue from 'vue';
import axios from 'axios';


Vue.prototype.http = axios.create({
  baseURL: process.env.MIX_BASE_URL + 'rest/',
  headers: {
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': Vue.prototype.$CSRF
  },
  transformRequest: data => {
    Vue.prototype.$load(1);
    return JSON.stringify(data);
  },
  transformResponse: data => {
    Vue.prototype.$load();
    return JSON.parse(data);
  },
});


Vue.prototype.http.interceptors.response.use(
  response => {
    if ( response.data.message ) Vue.prototype.$msg(response.data.message, 1);
    return response.data;
  },
  error => {
    if ( error.response.data.message ) Vue.prototype.$msg(error.response.data.message, 0);
    throw new Error('Response');
  }
);
