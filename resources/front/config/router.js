import Vue      from 'vue';
import Router   from 'vue-router';
import store    from './store'

import Home     from '../view/home';
import Profile  from '../view/profile';

Vue.use(Router);

let router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  linkExactActiveClass: '',
  base: process.env.MIX_BASE_URL,
  routes: [
    { path: '/',                name: 'Home',           component: Home },
    { path: '/profile',         name: 'Profile',        component: Profile,   meta: { user: 1 } },
    { path: '*',                name: 'Page not found', redirect: '/' }
  ]
});


router.beforeEach((to, from, next) => {
  if ( store.state.user === null || Vue.prototype.$access(to) ) next();
});


export default router;
