import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    load: 0
  },

  mutations: {
    user ( state, value = false ) {
      state.user = typeof value === 'object' ? value : false;
    },
    load ( state, value ) {
      state.load += value === 1 ? 1 : -1;
      if ( state.load < 0 ) state.load = 0;
    }
  },

  actions: {}
});