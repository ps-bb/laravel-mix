import Vue from 'vue';
import store from './store';
import router from './router';

Vue.prototype.$user = () => store.state.user;

Vue.prototype.$CSRF = document.querySelector('meta[name="csrf_token"]').content;

/**
 * Global loader popup for ajax request
 *
 * @param num {number}
 */
Vue.prototype.$load = (num = -1) => {
  store.commit('load', num === 1 ? 1 : -1);
  if ( store.state.load > 0 ) {
    document.addEventListener('keydown', blockEvent);
    document.addEventListener('click', blockEvent);
  } else {
    document.removeEventListener('keydown', blockEvent);
    document.removeEventListener('click', blockEvent);
  }
};

const blockEvent = e => {
  e.preventDefault();
  return false;
};

/**
 * Simple notification
 *
 * @param {string|null} message
 * @param {int} type - 0 error | 1 success
 */
Vue.prototype.$msg = (message = null, type = 0) => {
  if ( !message || typeof message !== 'string' ) message = 'Something went wrong.';
  Vue.prototype.$message[type !== 1 ? 'error' : 'success'](message, 5);
};

/**
 * Route permission validation
 *
 * @return boolean
 */
Vue.prototype.$access = to => {
  let user  = store.state.user,
      meta  = to.matched[0].meta,
      auth  = meta.user === 1,
      guest = meta.user === 0;
  if ( user === null ) return false;

  if ( guest && user ) {
    router.push('/');
    return false;
  }
  if ( auth && !user ) {
    router.push('/');
    return false;
  }

  document.title = to.name;
  return true;
};