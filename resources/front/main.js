import               './config/ant-design'
import               './config/prototypes'
import               './config/http'
import store    from './config/store'
import router   from './config/router'
import Vue      from 'vue'
import App      from './view/App'

document.body.innerHTML = '<div id=app></div>';

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  mounted () {
    Vue.prototype.http.get('user').then(data => {
      store.commit( 'user', ( !data || !data.id ) ? false : data );
      Vue.prototype.$access(router.currentRoute);
    }).catch( () => {
      store.commit('user');
      Vue.prototype.$access(router.currentRoute);
    });
  }
});
