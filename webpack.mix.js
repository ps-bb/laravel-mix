let mix = require('laravel-mix');
let webpack = require('webpack');
require('laravel-mix-purgecss');



mix
.webpackConfig({
  resolve: {
    modules: [
      path.resolve('./resources/front'),
      path.resolve('./node_modules')
    ]
  }
})
.js('resources/front/main.js', 'public/assets/app.js')
.sass('resources/front/style/default.scss', 'public/assets/app.css');



if ( mix.inProduction() ) {
  let credential = require('fs').readFileSync('.credential', 'utf8');
  mix
  .webpackConfig({
    plugins: [
      new webpack.BannerPlugin(credential)
    ]
  })
  .options({
    terser: {
      terserOptions: {
        compress: { drop_console: true }
      }
    },
    extractVueStyles: true
  })
  /*.purgeCss()*/;
}