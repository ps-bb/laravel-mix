<?php

use Faker\Generator as Faker;

$factory->define(App\Models\User::class, function (Faker $faker) {
	return [
		'email' => $faker->unique()->safeEmail,
		'rank' => $faker->numberBetween(2, 3),
		'logo' => null,
		'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
		'remember_token' => null
	];
});
