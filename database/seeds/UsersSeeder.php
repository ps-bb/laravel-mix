<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{

	public function run()
	{

		DB::table('users')->insert([
		    [
			    'email' => 'ps.not.use@gmail.com',
			    'role' => 'Admin',
			    'password' => bcrypt('11111111')
		    ],
			[
				'email' => 'ps.dev.smtp@gmail.com',
				'role' => 'Moderator',
				'password' => bcrypt('11111111')
			],
			[
				'email' => 'mix-test@gmail.com',
				'role' => 'Member',
				'password' => bcrypt('11111111')
			]
	    ]);

    }

}
