<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserFrForgot extends FormRequest
{

	protected function failedValidation(Validator $validator) {
		throw new HttpResponseException(response()->json([
			'message' => $validator->errors()->first()
		], 422));
	}

    public function rules()
    {
	    return [
		    'email' => 'required|email|max:255',
	    ];
    }

	public function messages()
	{
		return [
			'email.unique'          => 'Email address already exists',
			'email.*'               => 'Email address is not valid',
		];
	}

}