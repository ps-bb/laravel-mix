<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserFrLogo extends FormRequest
{

	protected function failedValidation(Validator $validator) {
		throw new HttpResponseException(response()->json([
			'message' => $validator->errors()->first()
		], 422));
	}

    public function rules()
    {
	    return [
		    'logo' => 'nullable|file|mimes:jpeg,jpg,png,svg,gif|max:200',
	    ];
    }

	public function messages()
	{
		return [
			'logo.*'               => 'Logo must be jpg or png and no more 200kb'
		];
	}

}