<?php

namespace App\Http\Controllers;

use App\Core\UserCore;
use App\Http\Requests\User\ {
	UserFrForgot,
	UserFrLogo,
	UserFrPassword,
	UserFrReset,
	UserFrSignUp
};

class UserController
{

	/**
	 * Get user data.
	 *
	 * @return mixed;
	 */
	function get()
	{
		return auth()->user() ?? [];
	}

	/** Logout current user */
	function logout()
	{
		return auth()->logout();
	}

	/**
	 * User Authorization
	 *
	 * @param UserCore $userCore
	 * @return array
	 */
	function signIn(UserCore $userCore)
	{
		if ( $userCore->attempt( request('email'), request('password') ) ) {
			return $this->get();
		}

		return abort(422, 'These credentials do not match our records.');
	}

	/**
	 * Registration new user
	 *
	 * @param UserFrSignUp $fr
	 * @param UserCore $userCore
	 * @return array
	 */
	function signUp(UserFrSignUp $fr, UserCore $userCore)
	{
		$result = $userCore->signUp(request('email'), request('password'));
		if ( $result ) {
			auth()->loginUsingId($result->id, true);

			return $this->get();
		}

		return abort(422, UNKNOWN);
	}

	/**
	 * Create request for reset user password
	 *
	 * @param UserFrForgot $fr
	 * @param UserCore $userCore
	 * @return array
	 */
	function forgot(UserFrForgot $fr, UserCore $userCore)
	{
		return $userCore->sendResetPassword(request('email'))
			? [ 'message' => 'We have e-mailed your password reset link!' ]
			: abort(422, "Email address doesn't exist");
	}

	/**
	 * Confirm reset user password.
	 *
	 * @param UserFrReset $fr
	 * @param UserCore $userCore
	 * @return array
	 */
	function reset(UserFrReset $fr, UserCore $userCore)
	{
		$data = request()->toArray();

		$reset = $userCore->resetPassword($data);

		if ( !$reset ) {

			$userCore->attempt($data['email'], $data['password']);

			return $this->get();
		}

		return abort(422, $reset);
	}

	/**
	 * Update user password.
	 *
	 * @param UserFrPassword $fr
	 * @param UserCore $userCore
	 * @return array
	 */
	function password(UserFrPassword $fr, UserCore $userCore)
	{
		if ( !$userCore->setPassword(request('password')) )
			abort(422, UNKNOWN);

		auth()->loginUsingId(auth()->id(), true);

		return [
			'message' => 'Password successfully changed',
			'user' => auth()->user()
		];
	}

	/**
	 * Update|Delete user logo.
	 *
	 * @param UserFrLogo $fr
	 * @param UserCore $userCore
	 * @return array
	 */
	function logo(UserFrLogo $fr, UserCore $userCore)
	{
		if ( !request()->file('logo') ) {

			if ( $userCore->deleteLogo() )
				return [ 'message' => 'Logo successfully deleted' ];

		} else {

			if ( $userCore->uploadLogo(request()->file('logo')) )
				return [ 'message' => 'Logo successfully changed' ];

		}

		abort(422, UNKNOWN);
	}

}