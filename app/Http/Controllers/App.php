<?php

namespace App\Http\Controllers;

class App {

	function view()
	{
		return view('app');
	}

	function notFound()
	{
		abort(404);
	}

}