<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    if ( !defined('UNKNOWN') ) {
		    define('UNKNOWN', 'Something went wrong. Try again.');
	    }
	    #\DB::listen(function($item) { echo $item->sql ."\n"; });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
