<?php

namespace App\Core;

use Illuminate\Support\Facades\Password;
use App\Model\User;

class UserCore {

	/**
	 * Return valid role name
	 *
	 * @param string|null $name
	 * @return string
	 */
	public function role(?string $name) : string
	{
		return in_array($name, [ 'Admin', 'Moderator', 'Member' ])
			? $name : 'Member';
	}

	/**
	 * Returns one of the fields of the specified user.
	 *
	 * @param string $key - which key of data do you need
	 * @param int|null $id - user id or current user
	 * @return string|null
	 */
	public function get(string $key = 'id', ?int $id = null) : ?string
	{
		$user = $id
			? User::find($id)
			: auth()->user();

		if ( !$user ) return null;

		return $user->{$key} ?? null;
	}

	/**
	 * Attempt to authenticate a user with the specified keys.
	 *
	 * @param string $email - user email address
	 * @param string $password - user password
	 * @return bool
	 */
	public function attempt(string $email, string $password) : bool
	{
		return auth()->attempt([
			'email' => $email,
			'password' => $password
		], true);
	}

	/**
	 * Create new user
	 *
	 * @param string $email - user email address
	 * @param string $password - user password
	 * @param string|null $role - user role or set default
	 * @return User|null
	 */
	public function signUp(string $email, string $password, ?string $role = null) : ?User
	{
		return User::create([
			'email' => $email,
			'password' => bcrypt($password),
			'role' => $this->role($role)
		]);
	}

	/**
	 * Send reset link to email
	 *
	 * @param string $email - user email address
	 * @return bool
	 */
	public function sendResetPassword(string $email) : bool
	{
		$reset = Password::broker()
		                 ->sendResetLink([ 'email' => $email ]);

		return Password::RESET_LINK_SENT == $reset;
	}

	/**
	 * Returns the text of the error or null if all is well.
	 *
	 * @param array $data - array of reset data (token, email, new password and confirmation password)
	 * @return bool|string
	 */
	public function resetPassword(array $data) : ?string
	{
		$reset = Password::broker()->reset( $data, function ($user, $password) {
			$user->password = bcrypt($password);
			$user->remember_token = null;
			$user->save();
		});

		if ( Password::PASSWORD_RESET == $reset)
			return null;

		return __($reset) ?? UNKNOWN;
	}

	/**
	 * Sets the new password to the specified user
	 *
	 * @param string $password - new password
	 * @param int|null $id - user id or current user
	 * @return bool
	 */
	public function setPassword(string $password, ?int $id = null) : bool
	{
		$user = User::find($id ?? auth()->id());
		if ( strlen($password) < 8 or !$user ) return false;

		$user->password = bcrypt($password);
		$user->save();

		return true;
	}

	/**
	 * Upload logo to the specified user
	 *
	 * @param $file - uploaded file
	 * @param int|null $id - user id or current user
	 * @return bool
	 */
	public function uploadLogo($file, ?int $id = null) : bool
	{
		$user = User::find($id ?? auth()->id());
		if ( !$user ) return false;

		$value = $id .'.'. $file->getClientOriginalExtension();
		if ( !$file->move( public_path(User::$logoFolder), $value ) )
			return false;

		$user->logo = $value .'?'. time();
		$user->save();

		return true;
	}

	/**
	 * Delete logo to the specified user
	 *
	 * @param int|null $id - user id or current user
	 * @return bool
	 */
	public function deleteLogo(?int $id = null) : bool
	{
		$user = User::find($id ?? auth()->id());
		if ( !$user ) return false;

		if ( $user->logo )
			\File::delete( public_path(User::$logoFolder . explode('?', $user->logo)[0]) );

		$user->logo = null;
		$user->save();

		return true;
	}
}