<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\ModelTrait;

class User extends Authenticatable
{
	use Notifiable, ModelTrait;

	protected $table = 'users';

	protected $fillable = [ 'email', 'logo', 'role', 'password' ];

	protected $hidden = [ 'password', 'remember_token' ];

	protected $appends = [ 'logoUrl' ];

	function getLogoUrlAttribute($value)
	{
		if ( !$value ) return null;

		return config('app.url') . self::$logoFolder . $value;
	}

	/**
	 * Location folder for user logos.
	 *
	 * @var string
	 * @example /media/user/{user.id}.{extension}
	 */
	static $logoFolder = '/media/user/';



}
