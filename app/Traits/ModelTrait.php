<?php

namespace App\Traits;

trait ModelTrait
{

	/** Change some configuration */
	function bootIfNotBooted() {
		parent::bootIfNotBooted();

		$this->timestamps = false;
	}

	/**
	 * Update first item
	 *
	 * @param array     $data   - data for update
	 * @param mixed     $value  - $key value
	 * @param string    $key    - identify by column
	 *
	 * @return void
	 */
	function singleUpdate($data, $value, $key = 'id')
	{
		$this->where($key, $value)
		     ->first()
		     ->update($data);
	}

}